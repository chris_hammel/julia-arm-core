include("julia-arm-core.jl")
using .julia_arm_core


function main()
    run_state_machine!(mySM)
    if mySM.state == 0
        if mySM.entering_state
            mySM.state_desc = "IDLE"
            println("$(mySM.swsource): $(mySM.state)")
        end
        if true #mySM.enabled
            mySM.state = 100
        end

    elseif mySM.state == 100
        if mySM.entering_state
            mySM.state_desc = "do stuff 100"
            println("$(mySM.swsource): $(mySM.state)")
        end
        if !mySM.flag
            println("setting FLAG to true")
            mySM.flag = true
        else
            mySM.state = 200
        end
    elseif mySM.state == 200
        if mySM.entering_state
            mySM.state_desc = "do stuff 200"
            println("$(mySM.swsource): $(mySM.state)")
        end
        mySM.state = 0
    else
        error("unknown state $(mySM.state)")
    end
    transition!(mySM)
end

mySM = StateMachine("test", true)
done_trig = F_TRIG()
global i
i = 0
function loop_until_done()
    while i < 100 && !done_trig.Q
        main()
        run_trigger!(done_trig, mySM.busy)
        global i
        # println(i)
        i += 1
    end
end

@time loop_until_done()
print("complete after $i scans")
