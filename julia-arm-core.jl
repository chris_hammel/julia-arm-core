#julia-arm-core.jl
module julia_arm_core
include("./Arrays/Arrays.jl")
using .ARM_Arrays
export prepend_fixed_length!

include("./Triggers/Triggers.jl")
using .ARM_Triggers
export R_TRIG, F_TRIG, run_trigger!

include("./StateMachine/StateMachine.jl")
using .ARM_StateMachine
export StateMachine, run_state_machine!, transition!

end
