module ARM_StateMachine
# include("../Triggers/Triggers.jl")
using ..julia_arm_core
export StateMachine, run_state_machine!, transition!
struct state_duration
    state::Int64
    scans_in_state::Int64
end
mutable struct StateMachine
    name::AbstractString
    post_transition_messages::Bool

    state::Int64
    entering_state::Bool
    exiting_state::Bool
    _scans_in_state::Int64

    _state_history::Array{state_duration}
    _state_start_of_scan::Int64
    _first_scan::Bool
    _prev_state::Int64
    enabled::Bool
    busy::Bool
    enable::Bool
    flag::Bool
    state_desc::String
    swsource::String
    _enabled_trig::R_TRIG

    StateMachine(
            name,
            post_transition_messages=false) = new(
                                                name,
                                                post_transition_messages,
                                                0,
                                                false,
                                                false,
                                                0,
                                                Array{state_duration}(undef, 3),
                                                0,
                                                true,
                                                0,
                                                false,
                                                false,
                                                false,
                                                false,
                                                "",
                                                "",
                                                R_TRIG())
end
function run_state_machine!(SM::T) where T <: StateMachine
    SM.enabled = run_trigger!(SM._enabled_trig, SM.enable);
    SM.busy = SM.state != 0;
    SM.swsource = "State $(SM.state)";
    SM._state_start_of_scan = SM.state;
    SM.exiting_state = false;
    SM.entering_state = (SM.state != SM._prev_state) || SM._first_scan;
    if SM.entering_state
        prepend_fixed_length!(SM._state_history, state_duration(SM._prev_state, SM._scans_in_state));
        SM._scans_in_state = 1;
        # add timing reset stuff here
    else
        SM._scans_in_state += 1;
        # add timing stuff here
    end
    if SM._first_scan
        SM._first_scan = false;
    end
    SM._prev_state = SM.state;
end
function transition!(SM::T) where T <: StateMachine
    if SM.state != SM._prev_state
        SM.flag = false;
        if SM.post_transition_messages
            println("($(SM.swsource)): Transition -> state $(SM.state) after $(SM._scans_in_state) scans")
        end
    end
    SM.exiting_state = false;
    SM.busy = SM.state != 0;
end
end
