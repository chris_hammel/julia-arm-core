module ARM_Triggers
export R_TRIG, F_TRIG, run_trigger!
abstract type Trigger end
mutable struct R_TRIG <: Trigger
    Q::Bool
    _prev_IN::Bool
    R_TRIG() = new(false, false)
end
mutable struct F_TRIG <: Trigger
    Q::Bool
    _prev_IN::Bool
    F_TRIG() = new(false, false)
end
function run_trigger!(Trig::R_TRIG, IN::Bool)
    if Trig.Q
        Trig.Q = false
    else
        if Trig._prev_IN
            Trig.Q = false
        else
            if IN
                Trig.Q = true
            else
                Trig.Q = false
            end
        end
    end
    Trig._prev_IN = IN
    return Trig.Q
end
function run_trigger!(Trig::F_TRIG, IN::Bool)
    if Trig.Q
        Trig.Q = false
    else
        if !Trig._prev_IN
            Trig.Q = false
        else
            if !IN
                Trig.Q = true
            else
                Trig.Q = false
            end
        end
    end
    Trig._prev_IN = IN
    return Trig.Q
end
end
