module ARM_core
export ARM_Triggers, ARM_StateMachine

module ARM_Triggers
export R_TRIG, F_TRIG
abstract type Trigger end
mutable struct R_TRIG <: Trigger
    Q::Bool
    _prev_IN::Bool
    Trigger() = new(false, false)
end
mutable struct F_TRIG <: Trigger
    Q::Bool
    _prev_IN::Bool
    Trigger() = new(false, false)
end
function run!(Trig::R_TRIG, IN::Bool)
    if Trig.Q
        Trig.Q = false
    else
        if Trig._prev_IN
            Trig.Q = false
        else
            if IN
                Trig.Q = true
            else
                Trig.Q = false
            end
        end
    end
    Trig._prev_IN = IN
    return Trig.Q
end
function run!(Trig::F_TRIG, IN::Bool)
    if Trig.Q
        Trig.Q = false
    else
        if !Trig._prev_IN
            Trig.Q = false
        else
            if !IN
                Trig.Q = true
            else
                Trig.Q = false
            end
        end
    end
    Trig._prev_IN = IN
    return Trig.Q
end
end



module ARM_StateMachine
using .ARM_Triggers
export StateMachine
struct state_duration
    state::Int64
    scans_in_state::Int64
end
mutable struct StateMachine
    name::AbstractString
    post_transition_messages::Bool

    state::Int64
    entering_state::Bool
    exiting_state::Bool
    _scans_in_state::Int64

    _state_history::Array{state_duration}
    _state_start_of_scan::Int64
    _first_scan::Bool
    _prev_state::Int64
    enabled::Bool
    busy::Bool
    enable::Bool
    flag::Bool
    state_desc::String
    swsource::String
    _enabled_trig::R_TRIG

    StateMachine(
            name,
            post_transition_messages=false) = new(
                                                name,
                                                post_transition_messages,
                                                0,
                                                false,
                                                false,
                                                0,
                                                Array{state_duration}(undef, 100),
                                                0,
                                                true,
                                                0,
                                                false,
                                                false,
                                                false,
                                                false,
                                                "",
                                                "",
                                                R_TRIG())
end
function run(SM::T) where T <: StateMachine
    SM.enabled = run!(SM._enabled_trig, SM.enable)
    SM.busy = SM.state != 0
    SM.swsource = "(State $SM.state)"
    SM._state_start_of_scan = SM.state
    SM.exiting_state = false
    SM.entering_state = (SM.state != SM._prev_state) || SM._first_scan
    if SM.entering_state
        insert!(self._state_history, 1, state_duration(SM._prev_state, SM._scans_in_state))
        SM._scans_in_state = 1
        # add timing reset stuff here
    else
        SM._scans_in_state += 1
        # add timing stuff here
    end
    if SM._first_scan
        SM._first_scan = false
    end
    SM._prev_state = SM.state
end
function transition(SM::T) where T <: StateMachine
    if SM.state != SM._prev_state
        SM.flag = false
    end
    SM.exiting_state = false
    SM.busy = SM.state != 0
end
end
end


function main()
    print("main function")
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
