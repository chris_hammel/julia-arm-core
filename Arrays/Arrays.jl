module ARM_Arrays
export prepend_fixed_length!
function prepend_fixed_length!(arr::Vector{T}, new_value::T) where {T}
    pushfirst!(arr, new_value)
    pop!(arr)
end

end
